package com.veripark.veriparktest.adapters;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.veripark.veriparktest.R;
import com.veripark.veriparktest.db.History;

public class HistoryAdapter extends BaseAdapter {

	private final Context mContext;
	private ViewHolder holder;
	private List<History> history;

	public HistoryAdapter(final Context context, List<History> history) {
		mContext = context;

		this.history = history;
	}

	@Override
	public int getCount() {
		return history.size();
	}

	@Override
	public Object getItem(int position) {
		return history.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {

		if (null == view) {
			view = LayoutInflater.from(mContext).inflate(R.layout.list_row,
					null);

			holder = new ViewHolder(view);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

	

		holder.lblChangeFrom.setText(history.get(position).getChangeFrom());
		holder.lblChangeTo.setText(history.get(position).getChangeTo());
		holder.lblAmount.setText(String.format("%.2f",(history.get(position).getAmount())));

		return view;
	}

	private static class ViewHolder {

		public final TextView lblChangeFrom;
		public final TextView lblChangeTo;
		public final TextView lblAmount;
		

		public ViewHolder(final View view) {
			lblChangeFrom = (TextView) view.findViewById(R.id.lbl_change_from);
			lblChangeTo = (TextView) view.findViewById(R.id.lbl_change_to);
			lblAmount = (TextView) view.findViewById(R.id.lbl_amount);
		}

	}

}
