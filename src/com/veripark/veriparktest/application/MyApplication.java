package com.veripark.veriparktest.application;

import android.app.Application;

import com.veripark.veriparktest.ActivityContextProvider;
import com.veripark.veriparktest.data.DataModel;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		ActivityContextProvider.setContext(this);
		DataModel.initDB();

	}

}
