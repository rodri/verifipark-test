package com.veripark.veriparktest.network;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;

@SuppressWarnings("rawtypes")
public class VolleyGsonRequest extends Request {

	private final Gson gson = new Gson();
	private final Class clazz;
	private final Listener listener;
	private String mRequestBody;
	private static final String PROTOCOL_CHARSET = "utf-8";
	@SuppressWarnings("unused")
	private Context context;

	public VolleyGsonRequest(int method, String url, Object body, Class clazz,
			Listener listener, ErrorListener errorListener, Context context) {
		super(method, url, errorListener);
		this.clazz = clazz;
		this.listener = listener;
		this.context = context;

		// convert from pojo to json
		if (body != null) {
			mRequestBody = gson.toJson(body);
		}
	}

	/*
	 * getting the body of our request in bytes
	 */
	@Override
	public byte[] getBody() {
		try {
			return mRequestBody == null ? null : mRequestBody
					.getBytes(PROTOCOL_CHARSET);
		} catch (UnsupportedEncodingException e) {
			VolleyLog
					.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
							mRequestBody, PROTOCOL_CHARSET);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Response parseNetworkResponse(NetworkResponse response) {
		try {
			String json = new String(response.data,
					MYHttpHeaderParser.parseCharset(response.headers));

			if (clazz == null) {
				Map<String, String> map = new HashMap<String, String>();

				return Response.success(gson.fromJson(json, map.getClass()),
						MYHttpHeaderParser.parseIgnoreCacheHeaders(response));
			} else {
				return Response.success(gson.fromJson(json, clazz),
						MYHttpHeaderParser.parseIgnoreCacheHeaders(response));
			}
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void deliverResponse(Object response) {
		listener.onResponse(response);
	}
}
