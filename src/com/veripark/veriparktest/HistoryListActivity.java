package com.veripark.veriparktest;

import java.util.List;

import com.veripark.veriparktest.adapters.HistoryAdapter;
import com.veripark.veriparktest.data.DataModel;
import com.veripark.veriparktest.db.History;
import android.app.ListActivity;
import android.os.Bundle;

public class HistoryListActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		
		List<History> history = DataModel.getHistory();
		
		HistoryAdapter adapter = new HistoryAdapter(this, history);
		setListAdapter(adapter);
	}
}
