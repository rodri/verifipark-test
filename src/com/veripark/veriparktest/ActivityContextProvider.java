package com.veripark.veriparktest;

import android.content.Context;
import android.content.res.Resources;

public class ActivityContextProvider {

	private static Context context;

	/**
	 * @return - The context of the main application. This can be used on any
	 *         instance or context of the application in a static way.
	 */
	public static Context getContext() {
		return context;
	}

	/**
	 * @param context
	 *            the context to set
	 */
	public static void setContext(Context context) {
		ActivityContextProvider.context = context;
	}

	/**
	 * @return - The resources of the main application. This can be used on any
	 *         instance or context of the application in a static way.
	 */
	public static Resources getApplicationResources() {
		return context.getResources();
	}

}
