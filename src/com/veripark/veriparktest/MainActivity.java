package com.veripark.veriparktest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.veripark.veriparktest.data.DataModel;
import com.veripark.veriparktest.data.ExchangeRequest;
import com.veripark.veriparktest.db.History;
import com.veripark.veriparktest.network.VolleyGsonRequest;

public class MainActivity extends Activity {

	private Spinner spFrom;
	private Spinner spTo;
	private Button btCalculate;
	private Button btHistory;
	private EditText etAmount;
	private TextView tvResultValue;
	private HashMap<String, String> currencyList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getOutlets();
		getCurrencyList();
	}

	private void getOutlets() {

		spFrom = (Spinner) findViewById(R.id.sp_from);

		spTo = (Spinner) findViewById(R.id.sp_to);

		btCalculate = (Button) findViewById(R.id.bt_calculate);

		btCalculate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				calculate();
			}
		});

		etAmount = (EditText) findViewById(R.id.et_amount);

		tvResultValue = (TextView) findViewById(R.id.tv_result_value);

		btHistory = (Button) findViewById(R.id.bt_history);

		btHistory.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				navigateToHistoryList();
			}
		});

	}

	private void getCurrencyList() {

		String url = "http://openexchangerates.org/currencies.json";

		// initialize request
		com.veripark.veriparktest.network.VolleyGsonRequest volleyRequest = new VolleyGsonRequest(
				Request.Method.GET, url, null, null, new Response.Listener() {
					@Override
					public void onResponse(Object response) {

						currencyList = (HashMap<String, String>) response;

						List<String> currencyKeys = new ArrayList<String>();

						for (String currencyKey : currencyList.keySet()) {
							currencyKeys.add(currencyKey);
						}

						Collections.sort(currencyKeys);

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								MainActivity.this,
								android.R.layout.simple_spinner_item,
								currencyKeys);
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spFrom.setAdapter(adapter);

						spTo.setAdapter(adapter);
					}

				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError arg0) {

					}

				}, this);

		// initialize queue
		RequestQueue queue = Volley.newRequestQueue(this);
		volleyRequest.setShouldCache(true);
		// make the call
		queue.add(volleyRequest);

	}

	private void calculate() {

		String url = "http://rate-exchange.appspot.com/currency?" + "from="
				+ spFrom.getSelectedItem().toString() + "&to="
				+ spTo.getSelectedItem().toString();

		if (etAmount.getText().toString().isEmpty()) {
			return;
		}

		final float amount = Float.parseFloat(etAmount.getText().toString());

		Log.d("FROM:::", spFrom.getSelectedItem().toString());
		Log.d("TO:::", spTo.getSelectedItem().toString());

		// initialize request
		com.veripark.veriparktest.network.VolleyGsonRequest volleyRequest = new VolleyGsonRequest(
				Request.Method.GET, url, null, ExchangeRequest.class,
				new Response.Listener() {
					@Override
					public void onResponse(Object response) {

						tvResultValue.setText(

						String.format("%.2f",
								(

								amount * ((ExchangeRequest) response)
										.getRateFormated())

						));

						History history = new History();
						history.setHistoryDate(new Date());
						history.setChangeFrom(spFrom.getSelectedItem()
								.toString());
						history.setChangeTo(spTo.getSelectedItem().toString());
						history.setAmount(amount);

						DataModel.insertNewHistory(history);
					}

				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError arg0) {

					}

				}, this);

		// initialize queue
		RequestQueue queue = Volley.newRequestQueue(this);
		volleyRequest.setShouldCache(true);
		// make the call
		queue.add(volleyRequest);
	}

	public void navigateToHistoryList() {

		Intent i = new Intent(this, HistoryListActivity.class);
		startActivity(i);
	}
}
