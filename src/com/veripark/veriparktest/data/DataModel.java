package com.veripark.veriparktest.data;

import java.util.List;

import android.database.sqlite.SQLiteDatabase;

import com.veripark.veriparktest.ActivityContextProvider;
import com.veripark.veriparktest.db.DaoMaster;
import com.veripark.veriparktest.db.DaoMaster.DevOpenHelper;
import com.veripark.veriparktest.db.DaoSession;
import com.veripark.veriparktest.db.History;
import com.veripark.veriparktest.db.HistoryDao;

public class DataModel {

	/**
	 * DataModel Singleton methods
	 */
	private static DataModel instance = null;
	private static final String dbName = "history";

	private static SQLiteDatabase db;
	private static DevOpenHelper helper;
	private static DaoMaster daoMaster;
	private static DaoSession daoSession;
	public static boolean updateUserScreenInfo = false;
	private static HistoryDao historyDao;

	public static DataModel getInstance() {
		if (instance == null) {
			instance = new DataModel();
		}
		return instance;
	}

	public static void initDB() {

		helper = new DaoMaster.DevOpenHelper(
				ActivityContextProvider.getContext(), dbName, null);
		db = helper.getWritableDatabase();

		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		historyDao = daoSession.getHistoryDao();
	}

	public static long insertNewHistory(History history) {
		return historyDao.insert(history);
	}

	public static List<History> getHistory() {
		return historyDao.loadAll();
	}
}
