package com.veripark.veriparktest.data;

public class ExchangeRequest {

	private String to;
	private String rate;
	private String from;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public float getRateFormated() {

		try {
			return Float.parseFloat(rate);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

}
